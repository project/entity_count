## Entity Count

Simple module that reports the count for each entity type.
Also reports the count and information per bundle. Useful
when keeping track on the number of entities that are
currently stored in the database and for performance reasons.

### Usage
Under "Administration > Reports" you will find the "Entity count"
link.

Click on it to get a list of entity types and their count and,
if the type has more than one bundle, a per bundle report.
