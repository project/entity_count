<?php

namespace Drupal\entity_count\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\Core\Utility\TableSort;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Entity count controller.
 */
class EntityCountController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * The constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    Renderer $renderer
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('renderer')
    );
  }

  /**
   * Get the entity count report.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return array
   *   Returns the report.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function entityCount(Request $request): array {
    $header = [
      ['data' => $this->t('Entity type')],
      ['data' => $this->t('Count'), 'field' => 'count'],
      ['data' => $this->t('Actions')],
    ];

    $rows = [];

    $output = [
      '#type' => 'table',
      '#header' => $header,
    ];

    $definitions = $this->entityTypeManager->getDefinitions();
    foreach ($definitions as $definition) {
      $entity_type = $definition->getLabel();
      $storage = $this->entityTypeManager->getStorage($definition->id());
      if (get_class($definition) == 'Drupal\Core\Config\Entity\ConfigEntityType') {
        $entities = $storage->loadMultiple();
        $count = count($entities);
      }
      else {
        $query = $storage->getAggregateQuery()->accessCheck(FALSE);
        $query->count();
        $count = $query->execute();
      }

      $bundle_info = $this->entityTypeBundleInfo->getBundleInfo($definition->id());
      if (count($bundle_info) > 1) {
        $actions_array = [
          '#type' => 'dropbutton',
          '#links' => [
            'foo' => [
              'title' => $this->t('Per bundle'),
              'url' => Url::fromRoute('entity_count.per_bundle', ['entity_type' => $definition->id()]),
            ],
          ],
        ];
        $actions = $this->renderer->render($actions_array);
      }
      else {
        $actions = '';
      }

      $row = [
        'data' => [
          'entity_type' => $entity_type,
          'count' => $count,
          'actions' => $actions,
        ],
      ];

      $rows[] = $row;
    }

    $order = TableSort::getOrder($header, $request);
    $sort = TableSort::getSort($header, $request);
    if (!empty($order) && $order['name'] == 'Count') {
      if (!empty($sort) && $sort == 'desc') {
        usort($rows, function ($a, $b) {
          return $b['data']['count'] <=> $a['data']['count'];
        });
      }
      else {
        usort($rows, function ($a, $b) {
          return $a['data']['count'] <=> $b['data']['count'];
        });
      }
    }
    else {
      usort($rows, function ($a, $b) {
        return strcmp($a['data']['entity_type'], $b['data']['entity_type']);
      });
    }

    $output['#rows'] = $rows;

    return $output;
  }

  /**
   * Get entity count report per bundle.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $entity_type
   *   The entity type.
   *
   * @return array
   *   Returns the report.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function perBundle(Request $request, string $entity_type): array {
    $header = [
      ['data' => $this->t('Bundle')],
      ['data' => $this->t('Count'), 'field' => 'count'],
    ];

    $rows = [];

    $output = [
      '#type' => 'table',
      '#header' => $header,
    ];

    $bundle_info = $this->entityTypeBundleInfo->getBundleInfo($entity_type);
    foreach ($bundle_info as $bundle_id => $bundle_data) {
      $bundle = $bundle_data['label'];
      $definition = $this->entityTypeManager->getDefinition($entity_type);

      $keys = $definition->getKeys();
      $bundle_key = is_null($keys['bundle']) ? 'bundle' : $keys['bundle'];

      $query = $this->entityTypeManager->getStorage($entity_type)->getQuery()->accessCheck(FALSE);
      $query->condition($bundle_key, $bundle_id);
      $query->count();
      $count = $query->execute();

      $row = [
        'data' => [
          'bundle' => $bundle,
          'count' => $count,
        ],
      ];

      $rows[] = $row;
    }

    $order = TableSort::getOrder($header, $request);
    $sort = TableSort::getSort($header, $request);
    if (!empty($order) && $order['name'] == 'Count') {
      if (!empty($sort) && $sort == 'desc') {
        usort($rows, function ($a, $b) {
          return $b['data']['count'] <=> $a['data']['count'];
        });
      }
      else {
        usort($rows, function ($a, $b) {
          return $a['data']['count'] <=> $b['data']['count'];
        });
      }
    }
    else {
      usort($rows, function ($a, $b) {
        return strcmp($a['data']['bundle'], $b['data']['bundle']);
      });
    }

    $output['#rows'] = $rows;

    return $output;
  }

}
